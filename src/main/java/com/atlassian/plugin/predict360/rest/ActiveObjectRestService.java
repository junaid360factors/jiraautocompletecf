package com.atlassian.plugin.predict360.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * A resource of message.
 */
@Path("/list")
public class ActiveObjectRestService {

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findUserByPrjKey(@QueryParam("prj_key") String prjKey )
    {
    	System.out.println(prjKey);
    	//Data4CF[] dtos=searchService.findByPrjKey(prjKey);
    	String json="[{\"label\": \"First Value\"}, {\"label\": \"Second Value\"},{\"label\": \"Third Value\", \"value\": \"third-value\"},{\"label\": \"Fourth Value\", \"value\": \"fourth-value\", \"img-src\": \"url/avatar.png\"}]";
    	//return Response.ok(dtos).build();
    	return Response.ok(json).build();
    }
}