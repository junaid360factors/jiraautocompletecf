package com.atlassian.plugin.predict360.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActiveObjectRestServiceModel {

    @XmlElement(name = "value")
    private String message;

    public ActiveObjectRestServiceModel() {
    }

    public ActiveObjectRestServiceModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}