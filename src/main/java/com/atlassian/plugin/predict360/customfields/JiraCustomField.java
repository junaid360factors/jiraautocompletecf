package com.atlassian.plugin.predict360.customfields;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;

@Scanned
public class JiraCustomField extends GenericTextCFType {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public JiraCustomField(
            @JiraImport CustomFieldValuePersister customFieldValuePersister,
            @JiraImport GenericConfigManager genericConfigManager,
            @JiraImport TextFieldCharacterLengthValidator textFieldCharacterLengthValidator,
            @JiraImport JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator , jiraAuthenticationContext);
    }

    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem)
    {
        Map<String, Object> params = super.getVelocityParameters(issue, field, fieldLayoutItem);
        try {
        	Project proj=getCurrentUserAssociatedProject();
        	if(proj!=null){
        		params.put("current_prjkey", proj.getKey());
        	}
		} catch (Exception e) {
		}
        return params;
    }

    private final Project getCurrentUserAssociatedProject() throws Exception {
    	Project project = ComponentAccessor.getComponent(UserProjectHistoryManager.class).getCurrentProject(Permissions.BROWSE, getCurrentLoggedInUser());
    	if(project == null) {
    		String errorMsg = dateFormat.format(new Date())+" In getCurrentUserAssociatedProject >> no project found";
    		System.out.println(errorMsg);
    		throw new NullPointerException(errorMsg);
    	}
    	return project;
    } 
	
    private final ApplicationUser getCurrentLoggedInUser() throws Exception {
		ApplicationUser applicationUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
		if(applicationUser == null) {
    		String errorMsg = dateFormat.format(new Date())+" In getCurrentLoggedInUser >> no logged-in user found";
    		System.out.println(errorMsg);
    		throw new NullPointerException(errorMsg);
		}
		return applicationUser;
	} 
    
}